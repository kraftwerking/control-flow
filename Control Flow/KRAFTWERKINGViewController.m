//
//  KRAFTWERKINGViewController.m
//  Control Flow
//
//  Created by RJ Militante on 1/2/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGViewController.h"

@interface KRAFTWERKINGViewController ()

@end

@implementation KRAFTWERKINGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    int truckSpeed = 45;
    int lamboSpeed = 120;
    int carSpeed = 80;
    int mySpeed = carSpeed;
    
    //<> <= >= != ==
    if (mySpeed < 70) {
        NSLog(@"Keep cruising");
    }
    else if (mySpeed > 55 && mySpeed < 90) {
        NSLog(@"Cruising down highway 66");
    }
    else {
        NSLog(@"Slow down");
    }
    
    BOOL isTelevisionOn = NO;
    
    if(isTelevisionOn != YES) {
        NSLog(@"We should take a break soon and do some coding");
    }
    else {
        NSLog(@"Great job keep up the hard work");
    }
    
//    for (int meditationHours = 1; meditationHours <= 100; meditationHours ++) {
//        NSLog(@"I am getting more enlightened");
//    }
    
    int strandOfWheat = 1;
    for (int i = 2; i <= 30; i ++) {
        strandOfWheat = strandOfWheat * 2;
        NSLog(@"%i", strandOfWheat);
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
