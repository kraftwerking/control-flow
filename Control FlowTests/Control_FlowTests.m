//
//  Control_FlowTests.m
//  Control FlowTests
//
//  Created by RJ Militante on 1/2/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Control_FlowTests : XCTestCase

@end

@implementation Control_FlowTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
